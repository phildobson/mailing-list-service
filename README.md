# Mailing List
[![build status](https://gitlab.com/phildobson/mailing-list-service/badges/master/build.svg)](https://gitlab.com/phildobson/mailing-list-service/commits/master)<br />
[![coverage report](https://gitlab.com/phildobson/mailing-list-service/badges/master/coverage.svg)](https://gitlab.com/phildobson/mailing-list-service/commits/master)

## Synopsis
This is a simple microservice that offers features pertaining to a mailing list.

## Installation
Clone the project, and run `gradlew build`.
The Gradle wrapper is included in source control.

## API Reference
The microservice has a RESTful API for a 'mailinglistmember' resource.
There are currently two supported operations on this resource:
* Subscribing to the mailing list (POST)
* Unsubscribing from the mailing list (DELETE)

## Technology
* Spring Boot
* Spring MVC to expose RESTful endpoints
* Spring Data JPA with Hibernate (embedded H2 database for integration tests)