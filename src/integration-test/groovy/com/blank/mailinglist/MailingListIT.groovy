package com.blank.mailinglist

import com.blank.mailinglist.service.AlreadySubscribedException
import com.blank.mailinglist.service.MailingListMemberService
import com.blank.mailinglist.service.NotSubscribedException
import com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity
import com.blank.mailinglist.utils.web.MailingListServiceTestRestTemplate
import com.blank.utils.persistence.EntityTruncator
import com.blank.utils.web.TestErrorAttributes
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.validation.ConstraintViolationException

@SpringBootTest(classes = [TestContext],
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
class MailingListIT extends Specification {

    static final String EMAIL = "user@blank.com"
    static final String INVALID_EMAIL = "user"
    static final String NO_MESSAGE_AVAILABLE = "No message available"

    @Inject
    MailingListServiceTestRestTemplate testRestTemplate
    @Inject
    MailingListMemberService mailingListMemberService
    @Inject
    EntityTruncator transactionExecutor
    @Inject
    EntityTruncator entityTruncator

    def "User successfully subscribing"() {
        when:
        ResponseEntity subscription1ResponseEntity = subscribe(EMAIL)

        then:
        assertSuccessful(subscription1ResponseEntity)
        mailingListMemberService.getSubscribedEmails() == [EMAIL]
    }

    def "Invalid email subscribing fails"() {
        when:
        ResponseEntity subscription1ResponseEntity = subscribe(INVALID_EMAIL)

        then:
        assertException(subscription1ResponseEntity, ConstraintViolationException, NO_MESSAGE_AVAILABLE)
        mailingListMemberService.getSubscribedEmails() == []
    }

    def "Same user subscribing twice"() {
        when:
        ResponseEntity subscription1ResponseEntity = subscribe(EMAIL)
        ResponseEntity subscription2ResponseEntity = subscribe(EMAIL)

        then:
        assertSuccessful(subscription1ResponseEntity)
        assertException(subscription2ResponseEntity, AlreadySubscribedException, EMAIL)
        mailingListMemberService.getSubscribedEmails() == [EMAIL]
    }

    def "Non subscribed user trying to unsubscribe"() {
        when:
        ResponseEntity responseEntity = unsubscribe(EMAIL)

        then:
        assertException(responseEntity, NotSubscribedException, EMAIL)
        mailingListMemberService.getSubscribedEmails() == []
    }

    def "Invalid email unsubscribing fails"() {
        when:
        ResponseEntity subscription1ResponseEntity = unsubscribe(INVALID_EMAIL)

        then:
        assertException(subscription1ResponseEntity, ConstraintViolationException, NO_MESSAGE_AVAILABLE)
        mailingListMemberService.getSubscribedEmails() == []
    }

    def "Subscribed user can unsubscribe"() {
        when:
        ResponseEntity subscribeResponseEntity = subscribe(EMAIL)
        ResponseEntity firstUnsubscribeResponseEntity = unsubscribe(EMAIL)

        then:
        assertSuccessful(subscribeResponseEntity)
        assertSuccessful(firstUnsubscribeResponseEntity)
        mailingListMemberService.getSubscribedEmails() == []
    }

    def "Subscribed user trying to unsubscribe multiple times"() {
        when:
        ResponseEntity subscribeResponseEntity = subscribe(EMAIL)
        ResponseEntity firstUnsubscribeResponseEntity = unsubscribe(EMAIL)
        ResponseEntity secondUnsubscribeResponseEntity = unsubscribe(EMAIL)

        then:
        assertSuccessful(subscribeResponseEntity)
        assertSuccessful(firstUnsubscribeResponseEntity)
        assertException(secondUnsubscribeResponseEntity, NotSubscribedException, EMAIL)
        mailingListMemberService.getSubscribedEmails() == []
    }

    def cleanup() {
        entityTruncator.truncateEntity(MailingListMemberEntity)
    }

    private ResponseEntity<TestErrorAttributes> subscribe(String email) {
        return testRestTemplate.sendHttpRequestForMailingListMember(email, HttpMethod.POST)
    }

    private ResponseEntity<TestErrorAttributes> unsubscribe(String email) {
        return testRestTemplate.sendHttpRequestForMailingListMember(email, HttpMethod.DELETE)
    }

    private static void assertSuccessful(ResponseEntity responseEntity) {
        assert responseEntity.statusCode == HttpStatus.OK
        assert responseEntity.body == null
    }

    private
    static void assertException(ResponseEntity<TestErrorAttributes> responseEntity, Class exceptionType, String message) {
        assert responseEntity.statusCode == HttpStatus.INTERNAL_SERVER_ERROR
        TestErrorAttributes errorAttributes = responseEntity.getBody()
        assert errorAttributes.exception == exceptionType
        assert errorAttributes.message == message
    }

    @Configuration
    @Import(MailingListServiceContext)
    static class TestContext {

        @PersistenceContext
        EntityManager entityManager

        @Bean
        public EntityTruncator entityTruncator() {
            return new EntityTruncator(entityManager)
        }

        @Bean
        public MailingListServiceTestRestTemplate mailingListServiceTestRestTemplate(TestRestTemplate testRestTemplate) {
            return new MailingListServiceTestRestTemplate(testRestTemplate);
        }

    }

}