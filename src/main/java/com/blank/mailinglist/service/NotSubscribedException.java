package com.blank.mailinglist.service;

public class NotSubscribedException extends RuntimeException {

    public NotSubscribedException(String emailAddress) {
        super(emailAddress);
    }

}