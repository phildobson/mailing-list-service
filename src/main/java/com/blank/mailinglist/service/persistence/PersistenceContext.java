package com.blank.mailinglist.service.persistence;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;

import javax.persistence.EntityManager;

@Configuration
@EnableAutoConfiguration
@EntityScan("com.blank.mailinglist.service.persistence.entities")
public class PersistenceContext {

    @javax.persistence.PersistenceContext
    private EntityManager entityManager;

    @Bean
    public MailingListMemberRepository mailingListMemberRepository(EntityManager entityManager) {
        JpaRepositoryFactory jpaRepositoryFactory = new JpaRepositoryFactory(entityManager);
        return jpaRepositoryFactory.getRepository(MailingListMemberRepository.class);
    }

}