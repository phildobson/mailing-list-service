package com.blank.mailinglist.service.persistence.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class MailingListMemberEntity {

    @Id
    private String emailAddress;
    @Enumerated(EnumType.STRING)
    private SubscriptionStatus subscriptionStatus;

    public MailingListMemberEntity() {
    }

    public MailingListMemberEntity(String emailAddress) {
        this.emailAddress = emailAddress;
        this.subscriptionStatus = SubscriptionStatus.SUBSCRIBED;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public SubscriptionStatus getSubscriptionStatus() {
        return subscriptionStatus;
    }

    public void setSubscriptionStatus(SubscriptionStatus subscriptionStatus) {
        this.subscriptionStatus = subscriptionStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MailingListMemberEntity that = (MailingListMemberEntity) o;

        return emailAddress.equals(that.emailAddress);

    }

    @Override
    public int hashCode() {
        return emailAddress.hashCode();
    }
}