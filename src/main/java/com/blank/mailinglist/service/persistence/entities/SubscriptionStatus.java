package com.blank.mailinglist.service.persistence.entities;

public enum SubscriptionStatus {

    SUBSCRIBED,
    UNSUBSCRIBED

}