package com.blank.mailinglist.service.persistence;

import com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MailingListMemberRepository extends CrudRepository<MailingListMemberEntity, String> {

    @Query("SELECT COUNT(*) = 1 " +
            "FROM com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity m " +
            "WHERE m.emailAddress = :emailAddress " +
            "AND m.subscriptionStatus = com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.SUBSCRIBED")
    boolean isSubscribed(@Param("emailAddress") String emailAddress);

    @Query("SELECT m " +
            "FROM com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity m " +
            "WHERE m.subscriptionStatus = com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.SUBSCRIBED")
    List<MailingListMemberEntity> findSubscribed();

}