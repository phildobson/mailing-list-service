package com.blank.mailinglist.service;

import com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity;
import com.blank.mailinglist.service.persistence.MailingListMemberRepository;
import com.blank.mailinglist.service.persistence.entities.SubscriptionStatus;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MailingListMemberService {

    private MailingListMemberRepository mailingListMemberRepository;

    public MailingListMemberService(MailingListMemberRepository mailingListMemberRepository) {
        this.mailingListMemberRepository = mailingListMemberRepository;
    }

    @Transactional
    public void subscribe(String emailAddress) {
        if (!mailingListMemberRepository.exists(emailAddress)) {
            addToMailingList(emailAddress);
        } else {
            if (!mailingListMemberRepository.isSubscribed(emailAddress)) {
                updateStatus(emailAddress, SubscriptionStatus.SUBSCRIBED);
            } else {
                throw new AlreadySubscribedException(emailAddress);
            }
        }
    }

    private void updateStatus(String emailAddress, SubscriptionStatus subscriptionStatus) {
        MailingListMemberEntity mailingListMemberEntity = mailingListMemberRepository.findOne(emailAddress);
        mailingListMemberEntity.setSubscriptionStatus(subscriptionStatus);
    }

    private void addToMailingList(String emailAddress) {
        MailingListMemberEntity mailingListMemberEntity = new MailingListMemberEntity(emailAddress);
        mailingListMemberRepository.save(mailingListMemberEntity);
    }

    @Transactional
    public void unsubscribe(String emailAddress) {
        if (!mailingListMemberRepository.isSubscribed(emailAddress)) {
            throw new NotSubscribedException(emailAddress);
        }
        updateStatus(emailAddress, SubscriptionStatus.UNSUBSCRIBED);
    }

    @Transactional(readOnly = true)
    public List<String> getSubscribedEmails() {
        return StreamSupport.stream(mailingListMemberRepository.findSubscribed().spliterator(), true)
                .parallel()
                .map(MailingListMemberEntity::getEmailAddress)
                .collect(Collectors.toList());
    }

}