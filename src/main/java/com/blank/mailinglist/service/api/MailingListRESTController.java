package com.blank.mailinglist.service.api;

import com.blank.mailinglist.service.MailingListMemberService;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping(path = "mailinglistmember")
public class MailingListRESTController {

    private MailingListMemberService mailingListMemberService;

    public MailingListRESTController(MailingListMemberService mailingListMemberService) {
        this.mailingListMemberService = mailingListMemberService;
    }

    @RequestMapping(path = "{emailAddress:.*}", method = RequestMethod.POST)
    public void subscribe(@PathVariable @Valid @NotBlank @Email String emailAddress) {
        mailingListMemberService.subscribe(emailAddress);
    }

    @RequestMapping(path = "{emailAddress:.*}", method = RequestMethod.DELETE)
    public void unsubscribe(@PathVariable @Valid @NotBlank @Email String emailAddress) {
        mailingListMemberService.unsubscribe(emailAddress);
    }

}