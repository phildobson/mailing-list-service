package com.blank.mailinglist.service.api;

import com.blank.mailinglist.service.MailingListMemberService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class ApiContext {

    @Bean
    public MailingListRESTController restController(MailingListMemberService mailingListMemberService) {
        return new MailingListRESTController(mailingListMemberService);
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

}
