package com.blank.mailinglist.service;

import com.blank.mailinglist.service.persistence.MailingListMemberRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceContext {

    @Bean
    public MailingListMemberService mailingListService(MailingListMemberRepository mailingListMemberRepository) {
        return new MailingListMemberService(mailingListMemberRepository);
    }

}