package com.blank.mailinglist.service;

public class AlreadySubscribedException extends RuntimeException {

    public AlreadySubscribedException(String emailAddress) {
        super(emailAddress);
    }

}