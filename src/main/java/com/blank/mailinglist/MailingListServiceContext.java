package com.blank.mailinglist;

import com.blank.mailinglist.service.ServiceContext;
import com.blank.mailinglist.service.api.ApiContext;
import com.blank.mailinglist.service.persistence.PersistenceContext;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@EnableAutoConfiguration
@Import({ApiContext.class, ServiceContext.class, PersistenceContext.class})
public class MailingListServiceContext {
}