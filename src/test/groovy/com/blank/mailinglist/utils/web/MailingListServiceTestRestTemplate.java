package com.blank.mailinglist.utils.web;

import com.blank.utils.web.TestErrorAttributes;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public class MailingListServiceTestRestTemplate {

    private TestRestTemplate testRestTemplate;

    public MailingListServiceTestRestTemplate(TestRestTemplate testRestTemplate) {
        this.testRestTemplate = testRestTemplate;
    }

    public ResponseEntity<TestErrorAttributes> sendHttpRequestForMailingListMember(String email, HttpMethod httpMethod) {
        return testRestTemplate.exchange("/mailinglistmember/" + email, httpMethod, null, TestErrorAttributes.class);
    }

}
