package com.blank.mailinglist.service.api

import com.blank.mailinglist.service.MailingListMemberService
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.util.NestedServletException
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import javax.inject.Inject
import javax.validation.ConstraintViolationException

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

@SpringBootTest(classes = [TestContext])
class MailingListRESTControllerSpec extends Specification {

    static final String EMAIL = "user@blank.com"

    @Inject
    MailingListRESTController unit
    @Inject
    MailingListMemberService mailingListMemberService
    @Inject
    MockMvc mockMvc

    def "Valid email address subscribing calls to service for email to be subscribed"() {
        when:
        MvcResult mvcResult = mockMvc.perform(post("/mailinglistmember/${EMAIL}")).andReturn()

        then:
        mvcResult.getResponse().getStatus() == HttpStatus.OK.value()
        1 * mailingListMemberService.subscribe(EMAIL)
    }

    def "Invalid email address subscribing throws exception"() {
        when:
        mockMvc.perform(post("/mailinglistmember/rubbish"))

        then:
        NestedServletException nse = thrown(NestedServletException)
        nse.cause.class == ConstraintViolationException
        nse.cause.message == null
        0 * mailingListMemberService.subscribe(_)
    }

    def "Valid email address unsubscribing calls to service for email to be unsubscribed"() {
        when:
        MvcResult mvcResult = mockMvc.perform(delete("/mailinglistmember/${EMAIL}")).andReturn()

        then:
        mvcResult.getResponse().getStatus() == HttpStatus.OK.value()
        1 * mailingListMemberService.unsubscribe(EMAIL)
    }

    def "Invalid email address unsubscribing throws exception"() {
        when:
        mockMvc.perform(delete("/mailinglistmember/rubbish"))

        then:
        NestedServletException nse = thrown(NestedServletException)
        nse.cause.class == ConstraintViolationException
        nse.cause.message == null
        0 * mailingListMemberService.unsubscribe(_)
    }

    @Configuration
    @Import(ApiContext)
    static class TestContext {

        DetachedMockFactory detachedMockFactory = new DetachedMockFactory()

        @Bean
        MailingListMemberService mailingListMemberService() {
            return detachedMockFactory.Mock(MailingListMemberService)
        }

        @Bean
        MockMvc mockMvc(WebApplicationContext webApplicationContext) {
            return MockMvcBuilders.webAppContextSetup(webApplicationContext)
                    .build()
        }

    }

}