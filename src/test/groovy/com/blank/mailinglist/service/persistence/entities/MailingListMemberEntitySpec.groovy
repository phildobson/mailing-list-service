package com.blank.mailinglist.service.persistence.entities

import spock.lang.Specification

import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.SUBSCRIBED
import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.UNSUBSCRIBED

class MailingListMemberEntitySpec extends Specification {

    static final String EMAIL = "test"

    MailingListMemberEntity subscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: EMAIL, subscriptionStatus: SUBSCRIBED)
    MailingListMemberEntity unsubscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: EMAIL, subscriptionStatus: UNSUBSCRIBED)

    def "Email only constructor defaults status to subscribed"() {
        expect:
        new MailingListMemberEntity(EMAIL).subscriptionStatus == SUBSCRIBED
    }

    def "Objects with same email address are equal"() {
        expect:
        subscribedMailingListMemberEntity.equals(unsubscribedMailingListMemberEntity)
    }

    def "Objects with same email address have same hash code"() {
        expect:
        subscribedMailingListMemberEntity.hashCode() == unsubscribedMailingListMemberEntity.hashCode()
    }

}