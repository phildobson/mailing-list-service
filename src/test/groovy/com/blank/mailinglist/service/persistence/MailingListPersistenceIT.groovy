package com.blank.mailinglist.service.persistence

import com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import javax.inject.Inject
import javax.transaction.Transactional

import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.SUBSCRIBED
import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.UNSUBSCRIBED

@SpringBootTest(classes = PersistenceContext)
class MailingListPersistenceIT extends Specification {

    static final String SUBSCRIBED_EMAIL_ADDRESS = "iam@subscribed.com"
    static final String UNSUBSCRIBED_EMAIL_ADDRESS = "iam@unsubscribed.com"

    @Inject
    MailingListMemberRepository sut

    MailingListMemberEntity subscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: SUBSCRIBED_EMAIL_ADDRESS, subscriptionStatus: SUBSCRIBED)
    MailingListMemberEntity unsubscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: "unsubscribed", subscriptionStatus: UNSUBSCRIBED)

    @Transactional
    def "isSuscribed for subscribed email address returns true"() {
        setup:
        sut.save(subscribedMailingListMemberEntity)

        expect:
        sut.isSubscribed(SUBSCRIBED_EMAIL_ADDRESS)
    }

    @Transactional
    def "isSubscribed for unsubscribed email address returns false"() {
        setup:
        sut.save(unsubscribedMailingListMemberEntity)

        expect:
        !sut.isSubscribed(UNSUBSCRIBED_EMAIL_ADDRESS)
    }

    @Transactional
    def "isSubscribed for non subscribed email address returns false"() {
        expect:
        !sut.isSubscribed(SUBSCRIBED_EMAIL_ADDRESS)
    }

    @Transactional
    def "findSubscribed returns subscribed users list"() {
        setup:
        sut.save(subscribedMailingListMemberEntity)
        sut.save(unsubscribedMailingListMemberEntity)

        expect:
        sut.findSubscribed() == [subscribedMailingListMemberEntity]
    }

}