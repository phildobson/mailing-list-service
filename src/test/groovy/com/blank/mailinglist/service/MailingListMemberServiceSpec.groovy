package com.blank.mailinglist.service

import com.blank.mailinglist.service.persistence.MailingListMemberRepository
import com.blank.mailinglist.service.persistence.entities.MailingListMemberEntity
import spock.lang.Specification

import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.SUBSCRIBED
import static com.blank.mailinglist.service.persistence.entities.SubscriptionStatus.UNSUBSCRIBED

class MailingListMemberServiceSpec extends Specification {

    static final String EMAIL = "email"

    MailingListMemberRepository mailingListMemberRepository = Mock(MailingListMemberRepository)
    MailingListMemberService unit = new MailingListMemberService(mailingListMemberRepository)

    MailingListMemberEntity unsubscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: EMAIL, subscriptionStatus: UNSUBSCRIBED)
    MailingListMemberEntity subscribedMailingListMemberEntity = new MailingListMemberEntity(emailAddress: EMAIL, subscriptionStatus: SUBSCRIBED)

    def "Successful subscription of new user"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> false

        when:
        unit.subscribe(EMAIL)

        then:
        1 * mailingListMemberRepository.save(_ as MailingListMemberEntity) >> { args ->
            MailingListMemberEntity mailingListMemberEntity = args[0]
            assert mailingListMemberEntity.emailAddress == EMAIL
            assert mailingListMemberEntity.subscriptionStatus == SUBSCRIBED
        }
    }

    def "Successful resubscription of unsubscribed user"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> true
        mailingListMemberRepository.isSubscribed(EMAIL) >> false
        mailingListMemberRepository.findOne(EMAIL) >> unsubscribedMailingListMemberEntity

        when:
        unit.subscribe(EMAIL)

        then:
        unsubscribedMailingListMemberEntity.subscriptionStatus == SUBSCRIBED
    }

    def "Throws AlreadySubscribedException if user tries to resubscribe"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> true
        mailingListMemberRepository.isSubscribed(EMAIL) >> true

        when:
        unit.subscribe(EMAIL)

        then:
        AlreadySubscribedException e = thrown(AlreadySubscribedException)
        e.message == EMAIL
    }

    def "Allows a subscribed user to unsuscribe"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> true
        mailingListMemberRepository.isSubscribed(EMAIL) >> true
        mailingListMemberRepository.findOne(EMAIL) >> subscribedMailingListMemberEntity

        when:
        unit.unsubscribe(EMAIL)

        then:
        subscribedMailingListMemberEntity.subscriptionStatus == UNSUBSCRIBED
    }

    def "Throws NotSubscribedException if non existent user tries to unsubscribed"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> false

        when:
        unit.unsubscribe(EMAIL)

        then:
        NotSubscribedException e = thrown NotSubscribedException
        e.message == EMAIL
    }

    def "Throws NotSubscribedException is unsubscribed user tries to unsubscribed"() {
        setup:
        mailingListMemberRepository.exists(EMAIL) >> true
        mailingListMemberRepository.isSubscribed(EMAIL) >> false

        when:
        unit.unsubscribe(EMAIL)

        then:
        NotSubscribedException e = thrown NotSubscribedException
        e.message == EMAIL
    }

}