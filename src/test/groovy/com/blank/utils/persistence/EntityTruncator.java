package com.blank.utils.persistence;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;

public class EntityTruncator {

    private EntityManager entityManager;

    public EntityTruncator(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Transactional
    public <T> void truncateEntity(Class<T> entityType) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaDelete<T> query = builder.createCriteriaDelete(entityType);
        query.from(entityType);
        entityManager.createQuery(query)
                .executeUpdate();
    }

}
